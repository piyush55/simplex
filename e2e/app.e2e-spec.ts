import { SimplexFrontendPage } from './app.po';

describe('simplex-frontend App', function() {
  let page: SimplexFrontendPage;

  beforeEach(() => {
    page = new SimplexFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
