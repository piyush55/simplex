import { Component } from '@angular/core';
import { PhoneService } from './phone.service';
import { environment } from '../../environments/environment';
import { PhoneDetail } from './phone';

@Component({
  selector: 'app-phones',
  templateUrl: './phones.component.html',
  styleUrls: ['./phones.component.css'],
})

export class PhonesComponent {

  private phoneDetail: any;
  private errorMessage: any;
  private query: string;
  private isLoading: boolean;
  
  constructor(private phoneService: PhoneService) {
    this.isLoading = false;
  }

  search(event) {
    this.phoneDetail = null;
    this.errorMessage = '';
    this.isLoading = true;
    if(event) {
      this.phoneService.getPhone(this.query)
                      .subscribe(
                          data => {
                            if(this.query == event) {
                              this.phoneDetail = data;
                              this.isLoading = false;
                            }
                          },
                      error =>  {
                        if(this.query == event) {
                          this.errorMessage = <any>error;
                          this.isLoading = false;
                        }
                      });
    }
    else {
      this.isLoading = false;
    }
  }
}
