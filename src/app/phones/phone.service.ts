import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { PhoneDetail } from './phone';

@Injectable()
export class PhoneService {
  private phoneApiUrl = environment.apiUrl + '/api/phones.json';
  
  constructor(private http: Http) {
  }

  getPhone(query): Observable<PhoneDetail> {
    return this.http.get(this.phoneApiUrl+"?query="+query)
                    .map((res:Response) => res.json())
                    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}
